import 'package:flutter/material.dart';

import 'library_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'pick down here:',
            ),
            FutureBuilder<List<Library>>(
                future: getLibraries(),
                builder: (context, snapShot) {
                  if (snapShot.connectionState == ConnectionState.waiting)
                    return CircularProgressIndicator();
                  if (snapShot.connectionState == ConnectionState.done) {
                    if (snapShot.hasError)
                      return Center(
                        child: Text("error"),
                      );
                    if (snapShot.data != null) {
                      final items = snapShot.data;
                      Library selected = items![0];

                      return DropdownButton<Library>(
                        value: selected,
                        items: items.map((value) {
                          return DropdownMenuItem<Library>(
                            value: value,
                            child: Text(
                              value.title ?? "",
                              style: TextStyle(color: Colors.red),
                            ),
                          );
                        }).toList(),
                        onChanged: (val) {
                          setState(() {
                            selected = val ?? selected;
                          });
                        },
                      );
                    } else {
                      return Center(
                        child: Text("noo data"),
                      );
                    }
                  }

                  return Container();
                })
          ],
        ),
      ),
    );
  }

  Future<List<Library>> getLibraries() async {
    await Future.delayed(Duration(seconds: 3));
    return [
      Library(id: 1, title: 'i'),
      Library(id: 2, title: 'love'),
      Library(id: 3, title: 'banans'),
    ];
  }
}
